// Step 1: Import React
import * as React from 'react'
import Layout from '../components/layout'
import { bcgImage, loadImage, iconFolder, folders, folderName, folderName1 } from '../components/layout.module.css'
import { StaticImage } from 'gatsby-plugin-image'
import otterGIF from '../images/negroni.gif'
import { Link } from 'gatsby'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome' 
import { faFolderOpen } from '@fortawesome/free-regular-svg-icons'
import "@fontsource/micro-5"; // Defaults to weight 400
import "@fontsource/micro-5/400.css"; // Specify weight
import { body } from '../global.css'

// Step 2: Define your component
const IndexPage = () => {
  return (
    <div>
      <img src={otterGIF} alt="wait" className={loadImage} />
      <Layout></Layout> 
      <li className={folders}>
        <Link to="/fujifilm-xt-5" style={{ textDecoration: 'none', color: '#000' }}><span className={iconFolder}><FontAwesomeIcon icon={faFolderOpen}/></span></Link>
        <Link to="/rollei-35b" style={{ textDecoration: 'none', color: '#000' }}><span className={iconFolder}><FontAwesomeIcon icon={faFolderOpen}/></span></Link>
        <Link to="/fujifilm-xt-5" style={{ textDecoration: 'none', color: '#000' }}><span className={folderName}><p className={body}>fujifilm xt-5</p></span></Link>
        <Link to="/rollei-35b" style={{ textDecoration: 'none', color: '#000' }}><span className={folderName1}><p className={body}>rollei 35b</p></span></Link>
      </li>
      
    </div>
  )
}

// You'll learn about this in the next task, just copy it for now
export const Head = () => <title>Jiyu Fang</title>
                         

// Step 3: Export your component
export default IndexPage