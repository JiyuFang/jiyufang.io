import * as React from 'react'
import { Link } from 'gatsby'
import { navLinksLogo, navLinks, navLinksText, iconGitlab, iconDiscord, iconIns, iconXtwitter, iconLinkedin, folders } from './layout.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome' 
import { faXTwitter, faGitlab, faInstagram, faDiscord, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'
import { StaticImage } from 'gatsby-plugin-image'
import "@fontsource/micro-5"; // Defaults to weight 400
import "@fontsource/micro-5/400.css"; // Specify weight
import { body } from '../global.css'


const Layout = ({ pageTitle, children }) => {
  const CurrentDate = new Date().toLocaleString('default', {weekday:'short', month:'short', day:'numeric'}).replace(',', '\t')
  const CurrentTime = new Date().toLocaleString('default', {hour:'2-digit', minute:'2-digit', hour12:'true'}).replace('at', '\t')

  return (
    <div>
      <nav>
        <ul>
        <li className={navLinksLogo}><Link to="/"><StaticImage alt="me" src="../images/my-icon.png"/></Link></li>
        <li className={navLinks}>
        <a href="https://gitlab.com/JiyuFang" target="_blank" rel="noopener noreferrer"><span className={iconGitlab}><FontAwesomeIcon icon={faGitlab}/></span></a>
        <a href="https://discord.com/users/1074867832048857239" target="_blank" rel="noopener noreferrer"><span className={iconDiscord}><FontAwesomeIcon icon={faDiscord}/></span></a>
        <a href="https://www.instagram.com/iceyfongjy/" target="_blank" rel="noopener noreferrer"><span className={iconIns}><FontAwesomeIcon icon={faInstagram}/></span></a>
        <a href="https://twitter.com/JiyuFang" target="_blank" rel="noopener noreferrer"><span className={iconXtwitter}><FontAwesomeIcon icon={faXTwitter}/></span></a>
        <a href="https://www.linkedin.com/in/jiyu-fang/" target="_blank" rel="noopener noreferrer"><span className={iconLinkedin}><FontAwesomeIcon icon={faLinkedinIn}/></span></a>
        <a className={body}>{CurrentDate} {CurrentTime}</a>
        </li>
        <li className={folders}>
        {/*<span className={iconFolder}><FontAwesomeIcon icon={faFolderOpen}/>fujifilm xt-5</span>
        <span className={iconFolder}><FontAwesomeIcon icon={faFolderOpen}/>folder 2</span>
        <span className={iconFolder}><FontAwesomeIcon icon={faFolderOpen}/>folder 3</span> */}
        </li>
        </ul>
      </nav>
      <main>
        <h1>{pageTitle}</h1>
        {children}
      </main>
    </div>
  )
}

export default Layout